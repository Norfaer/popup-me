;
(function ($) {
    $.popupMe = function (element, options) {
        var self = this;
        var $element = $(element), $wrapper, wrapper, element = element, $loader;
        var defaults = {
            animationIn: 'bounceInLeft',
            animationOut: 'bounceOutRight',
            animationSpeed: 800,
            closeButton: true,
            closeOnContent: true,
            sourceType: 'html',
            sourceUrl: false,
            onBeforeShow: function () {},
            onShown: function () {},
            onBeforeHide: function () {},
            onHidden: function () {}
        };
        self.state = false;
        self.hasCloseButton = false;
        self.isAjax = false;
        self.options = {};
        self.init = function () {
            self.options = $.extend({}, defaults, options, $element.data());
            $element.addClass("popup-inner");
            $element.css('animation-duration', (self.options.animationSpeed/1000) + 's');
            if (!element.parentElement.classList.contains("popup")) {
                $element.wrap('<div class="popup"></div>');
            }
            $wrapper = $element.parent();
            wrapper = element.parentElement;
            if (self.options.sourceType === 'ajax' && self.options.sourceUrl) {           
                $loader = $('<div class="loader"><div class="loader-inner ball-pulse"><div></div><div></div><div></div></div></div>');
                $loader.appendTo($wrapper);
                self.isAjax = true;
            }
            else {
                self.button();
            }
            $wrapper.click(function (e) {
                e.preventDefault();
                if (wrapper === e.target && self.options.closeOnContent && self.state) {
                    self.close();
                }
            });
            $('[data-popup-open="#' + element.id + '"]').click(function (e) {
                e.preventDefault();
                self.open();
            });
        };
        self.open = function () {
            $('body').addClass('body-popup');
            if (self.isAjax) {
                $element.css('display', 'none');
                $wrapper.css('display', 'block');
                $loader.css('display','inline-block');
                $.ajax({
                    url:self.options.sourceUrl,
                    dataType:'html',
                    method:'GET'
                }).done(function(html) {
                    $element.html(html); 
                    self.button();
                    self.isAjax = false;
                    $loader.css('display','none');
                    $element.css('display', 'inline-block');
                    self.show();
                }).fail(function() {
                    //alert( "error" );
                }).always(function() {
                });                
            }
            else {
                self.show();
            }
        };
        self.close = function () {
            self.hide();
            $('body').removeClass('body-popup');
        };       
        self.show = function() {
            $wrapper.css('overflow','hidden');
            self.options.onBeforeShow();
            $element.trigger('pm_beforeshow', []);
            $wrapper.css('display', 'block');
            $element.addClass(self.options.animationIn);
            setTimeout(function(){
                $element.removeClass(self.options.animationIn);
                self.state = true;
                self.options.onShown();
                $element.trigger('pm_shown', []);  
                $('body').addClass('body-popup');
                $wrapper.css('overflow','auto');
            },self.options.animationSpeed);
        };        
        self.hide = function() {
            $wrapper.css('overflow','hidden');
            self.options.onBeforeHide();
            $element.trigger('pm_beforehide', []);
            $element.addClass(self.options.animationOut);
            setTimeout(function(){
                $element.removeClass(self.options.animationOut);                
                $wrapper.css('display', 'none');                
                self.state = false;
                self.options.onHidden();
                $element.trigger('pm_hidden', []);
                $('body').removeClass('body-popup');
                $wrapper.css('overflow','auto');
            },self.options.animationSpeed);         
        };        
        self.button = function() {
            if (self.options.closeButton && !self.hasCloseButton) {
                $('<a class="popup-close" data-popup-close="#' + element.id + '" href="#">&times;</a>').appendTo($element);
                self.hasCloseButton = true;
                $element.find('[data-popup-close="#' + element.id + '"]').click(function (e) {
                    e.preventDefault();
                    self.close();
                });                
            }
        };        
        self.set = function(opt) {
            self.options = $.extend( self.options, opt );
        };
        
        self.get = function(option) {
            return (typeof option === 'string') ? self.options[option] : self.options;
        };        
        self.init();
    };
    $.fn.popupMe = function (a, b) {
        return this.each(function () {
            var plugin = $(this).data("popupMe");
            if (undefined === plugin) {
                plugin = new $.popupMe(this, a);
                $(this).data("popupMe", plugin);
            }
            if (a === "open") {
                plugin.open();
            } else if (a === "close") {
                plugin.close();
            } else if (a === "set") {
                plugin.set(b);
            } else if (a === "get") {
                return plugin.get(b);
            }
        });
    };
    $(document).ready(function () {
        $(".popup-me").popupMe();
    });
}(jQuery));
